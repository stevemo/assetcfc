<cfcomponent displayname="Asset CFC" hint="Manage your assets" output="no">

	<!--- pseudo-constructor --->
	<cfscript>
		variables.instance = {
			css = arrayNew(1),
			js = arrayNew(1)
		};
	</cfscript>
	
	<!-- =============== -->
	<!-- ! init method   -->
	<!-- =============== -->
	<cffunction name="init" output="no" returnType="any" hint="Constructor for the assets component">
		<cfreturn this>
	</cffunction>
	
	<!-- =========================================================================================================== -->
	<!-- ! include css file to the css instance array 
		   accept array for multiple css files or string for 1 css file 
	<!-- =========================================================================================================== -->
	<cffunction name="setCss" hint="css file to be added to the array" output="no" returnType="void">
		<cfargument name="files" required="true" type="any">
		
		<!--- init local variable --->
		<cfset local = {} />
		<cfset var local.files = arguments.files />
		<cfset var local.link = '' />
		<cfset local.i = '' />
		
		<!--- if it's not a array convert it to array --->
		<cfif NOT isArray(local.files)>
			<cfset local.files = toArray(local.files) />
		</cfif>

		<cfloop from="1" to="#arrayLen(local.files)#" index="local.i">
			<cfset local.link = '<link href="#request.base##local.files[local.i]#" rel="stylesheet">'>
			<cfset arrayAppend(variables.instance.css, local.link) />
		</cfloop>
	
	</cffunction>
	
	<!-- ============================================== -->
	<!-- ! return css file to the layout			    -->
	<!-- ============================================== -->
	<cffunction name="getCss" hint="return css file added to the array surround with link tag" output="no" returnType="string">
		
		<!--- init local variable --->
		<cfset local = {} />
		<cfset local.rtn = '' />
		<cfset local.i = '' />
		
		<cfif arrayLen(variables.instance.css) GT 0>
			<cfloop from="1" to="#arrayLen(variables.instance.css)#" index="local.i">
				<cfset local.rtn &= variables.instance.css[local.i] />
			</cfloop>
		</cfif>
		
		<cfreturn local.rtn />
	</cffunction>
	
	<!-- =========================================================================================================== -->
	<!-- ! include js script file to the js instance array 
		   accept array for multiple js files or string for 1 js file 
	<!-- =========================================================================================================== -->
	<cffunction name="setJs" hint="js file to be added to the array" output="no" returnType="void">
		<cfargument name="files" required="true" type="any">
		
		<!--- init local variable --->
		<cfset local = {} />
		<cfset local.files = arguments.files />
		<cfset local.script = '' />
		<cfset local.i = '' />
		
		<!--- if it's not a array convert it to array --->
		<cfif NOT isArray(local.files)>
			<cfset local.files = toArray(local.files) />
		</cfif>

		<cfloop from="1" to="#arrayLen(local.files)#" index="local.i">
			<cfset local.script = '<script src="#request.base##local.files[local.i]#"></script>' />
			<cfset arrayAppend(variables.instance.js, local.script) />
		</cfloop>
	
	</cffunction>
	
	<!-- ============================================== -->
	<!-- ! return js file to the layout			    -->
	<!-- ============================================== -->
	<cffunction name="getJs" hint="return js file added to the array surround with script tag" output="no" returnType="string">
		
		<!--- init local variable --->
		<cfset local = {} />
		<cfset local.rtn = '' />
		<cfset local.i = '' />
		
		<cfif arrayLen(variables.instance.js) GT 0>
			<cfloop from="1" to="#arrayLen(variables.instance.js)#" index="local.i">
				<cfset local.rtn &= variables.instance.js[local.i] />
			</cfloop>
		</cfif>
		
		<cfreturn local.rtn />
	</cffunction>
	
	<!-- =========================== -->
	<!-- ! convert string to array   -->
	<!-- =========================== -->
	<cffunction name="toArray" access="private" output="no">
		<cfargument name="data" required="true" type="string">
		<cfreturn [arguments.data] />	
	</cffunction>

</cfcomponent>